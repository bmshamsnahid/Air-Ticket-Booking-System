import { AppStateService } from './services/app-state.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';

import {
  MatAutocompleteModule,  MatButtonModule,  MatButtonToggleModule,  MatCardModule,
  MatCheckboxModule,  MatChipsModule,  MatDatepickerModule,  MatDialogModule,
  MatExpansionModule,  MatGridListModule,  MatIconModule,  MatInputModule,
  MatListModule,  MatMenuModule,  MatNativeDateModule,  MatPaginatorModule,
  MatProgressBarModule,  MatProgressSpinnerModule,  MatRadioModule,  MatRippleModule,
  MatSelectModule,  MatSidenavModule,  MatSliderModule,  MatSlideToggleModule,
  MatSnackBarModule,  MatSortModule,  MatTableModule,  MatTabsModule,
  MatToolbarModule,  MatTooltipModule,  MatStepperModule } from '@angular/material';

import { HomeComponent } from './components/home/home.component';
import { SignInComponent } from './components/sign-in/sign-in.component';
import { SignUpComponent } from './components/sign-up/sign-up.component';
import { AppComponent } from './app.component';

import { AuthService } from './services/auth.service';
import { NavigationComponent } from './components/navigation/navigation.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SignInComponent,
    SignUpComponent,
    NavigationComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatSelectModule,
    MatButtonModule,
    MatMenuModule,
    MatCardModule,
    MatToolbarModule,
    MatIconModule,
    MatSidenavModule,
    MatInputModule,
    MatDialogModule,
    MatListModule,
    MatTabsModule,
  ],
  providers: [AuthService, AppStateService],
  bootstrap: [AppComponent]
})
export class AppModule { }
