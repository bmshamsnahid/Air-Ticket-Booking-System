export class AirplaneTouchLocation {
    locationId: String;
    time: String;
}

export class AirplaneDestinationLocation {
    locationId: String;
    time: String;
}

export class Airplane {
    airplanename: String;
    airplaneDepertureLocation: String;
    airplaneDestinationLocation: String;
    airplaneTouchLocation: AirplaneTouchLocation[];
    airplaneDestinationLocationTime: AirplaneDestinationLocation[];
    airplaneDayInWeeks: String[];
    isSeatAvailable: String;
    isEconomySeatAvailable: String;
    isBusinessClassAvailable: String;
    airplaneDescription: String;
}
