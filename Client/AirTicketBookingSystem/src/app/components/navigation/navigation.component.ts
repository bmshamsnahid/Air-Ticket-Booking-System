import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { AppStateService } from '../../services/app-state.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {

  isLoggedIn: boolean;

  constructor(private appStateService: AppStateService,
              private router: Router) {
    if ((typeof localStorage.getItem('myToken') !== 'undefined') || localStorage.getItem('myToken').length <= 0) {
      this.isLoggedIn = false;
      this.router.navigate(['/sign-in']);
    } else {
      this.isLoggedIn = true;
    }
  }

  ngOnInit() {
    this.appStateService.event.subscribe((data: String) => {
      if (data === 'true') {
        this.isLoggedIn = true;
      } else {
        this.isLoggedIn = false;
      }
    });
  }

  onClickLogOut() {
    localStorage.setItem('myToken', '');
    localStorage.setItem('profileId', '');
    this.router.navigate(['/sign-in']);
    const loginStatus = 'false';
    this.appStateService.publish(loginStatus);
  }

}
