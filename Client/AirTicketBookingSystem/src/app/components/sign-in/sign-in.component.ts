import { AppStateService } from './../../services/app-state.service';
import { AuthService } from './../../services/auth.service';
import { Customer } from './../../classes/customer';
import { Component, OnInit } from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {

  customer: Customer = new Customer();
  hide = true;

  constructor(private authService: AuthService,
              private appStateService: AppStateService,
              private router: Router) { }

  ngOnInit() {
  }

  onClickSignIn(): void {
    console.log(this.customer.customerEmail);
    console.log(this.customer.customerPassword);
    this.authService.getToken(this.customer).subscribe((response) => {
      console.log(response);

      if (response.success) {
        localStorage.setItem('myToken', response.token);
        localStorage.setItem('profileId', response.id);

        let loginStatus: any;
        loginStatus = 'true';

        this.appStateService.publish(loginStatus);

        this.router.navigate(['/home']);
      } else {
        console.log('Failed to get the token.');
      }
    });
  }

}
