import { Router, ActivatedRoute } from '@angular/router';
import { Customer } from './../../classes/customer';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  customer: Customer = new Customer();
  confirmPassword: String;
  hide = true;

  constructor(private authService: AuthService,
              private router: Router) { }

  ngOnInit() {
  }

  onClickSignUp(): void {
    console.log(this.customer.customerName);
    console.log(this.customer.customerEmail);
    console.log(this.customer.customerPassword);
    console.log(this.confirmPassword);
    if (this.isValidate()) {
      this.authService.createUser(this.customer).subscribe((response) => {
        console.log(response);
        this.router.navigate(['/sign-in']);
        // this.router.navigateByUrl('/sign-in');
      });
    } else {
      console.log('Error in grabbing the information');
    }
  }

  isValidate(): boolean {
    if ((typeof this.customer.customerName === 'undefined') || (this.customer.customerName.length <= 0)) {
      return false;
    } else if ((typeof this.customer.customerEmail === 'undefined') || (this.customer.customerEmail.length <= 0)) {
      return false;
    } else if ((typeof this.customer.customerPassword === 'undefined') || (this.customer.customerPassword.length <= 0)) {
      return false;
    } else if ( this.confirmPassword !== this.customer.customerPassword) {
      return false;
    }
    return true;
  }

}
