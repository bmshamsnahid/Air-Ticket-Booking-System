import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class AppStateService {

  constructor() { }

  data: any;

  public _subject = new Subject<object>();
  public event = this._subject.asObservable();

  public publish(data: String) {
    this.data = data;
    this._subject.next(this.data);
  }

}
