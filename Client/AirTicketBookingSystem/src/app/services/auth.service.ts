import { Customer } from './../classes/customer';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import {Http, RequestOptions, Headers, Response} from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class AuthService {
  apiUrl = environment.apiUrl;
  postUrl = this.apiUrl + '/' + 'customer';
  authUrl = this.apiUrl + '/' + 'auth';

  constructor(private http: Http) { }

  createUser(customer: Customer) {
    const headers = new Headers({'Content-Type': 'application/json'});
    const options = new RequestOptions({headers: headers});

    return this.http.post(this.postUrl, customer, options)
      .map((response: Response) => {
        return response.json();
      });
  }

  getToken(customer: Customer) {
    const headers = new Headers({'Content-Type': 'application/json'});
    const options = new RequestOptions({headers: headers});

    return this.http.post(this.authUrl, customer, options)
      .map((response: Response) => {
        return response.json();
      });
  }
}
