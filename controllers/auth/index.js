var jwt = require('jsonwebtoken');
var Customer = require('../../models/customer');
var configSecret = require('../../config/secret');

var authenticateUser = (req, res) => {
    console.log('Email: ' + req.body.customerEmail);
    console.log('Password: ' + req.body.customerPassword);
    
    Customer.find((err, customers) => {
        if (err) {
            res.status(404).json({
                message: 'error in getting all the databases: ' + err
            });
        } else {
            let index = 0;
            for (let customer of customers) {
                index = index + 1;
                if (req.body.customerEmail == customer.customerEmail) {
                    if (req.body.customerPassword == customer.customerPassword) {
                        var token = jwt.sign(customer, configSecret.secret, {
                            expiresIn: 1440  //24 hours
                        });
                        return res.status(200).json({
                            success: true,
                            message: 'Token Created !!!',
                            token: token,
                            id: customer._id
                        });
                    } else {
                        return res.status(500).json({
                            success: false,
                            message: 'Authentication failed: Password dose not match'
                        });
                    }
                }
            }
            if(index == customers.length) {
                res.status(404).json({
                    message: 'No user exists: '
                });
            }
        }
    });
};

var checkAuthentication = (req, res, next) => {
    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    if(token) {
        jwt.verify(token, configSecret.secret, (err, decoded) => {
            if(err) {
                return res.status(404).json({
                    success: false,
                    message: 'Failed to authenticate token'
                });
            } else {
                req.decoded = decoded;
                next();
            }
        });
    } else {
        return res.status(403).json({
            success: false,
            message: 'No Token Provided'
        });
    }
};

module.exports = {
    authenticateUser,
    checkAuthentication
};