var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
var Customer = require('../../models/customer');

var createCustomer = (req, res) => {
    var customer = new Customer(req.body);

    customer.save((err, savedCustomer) => {
        if (err) {
            res.status(404).json({
                message: 'Error in saving customer in database: ' + err
            });
        } else {
            res.status(200).json({
                customer: savedCustomer
            });
        }
    });
};

var getAllCustomer = (req, res) => {
    Customer.find((err, customers) => {
        if (err) {
            res.status(404).json({
                message: 'Error in getting all customers in database: ' + err
            });
        } else {
            res.status(200).json({
                customers: customers
            });
        }
    });
};

var getSingleCustomer = (req, res) => {
    Customer.findById(req.params.id, (err, customer) => {
        if (err) {
            res.status(404).json({
                message: 'Error in getting single customer in database: ' + err
            });
        } else {
            res.status(200).json({
                customer: customer
            });
        }
    });
};

var updateCustomer = (req, res) => {
    Customer.findById(req.params.id, (err, customer) => {
        if (err) {
            res.status(404).json({
                message: 'Error in getting single customer in database: ' + err
            });
        } else {
            customer.customerEmail = req.body.customerEmail || customer.customerEmail;
            customer.customerPassword = req.body.customerPassword || customer.customerPassword;
            customer.customerName = req.body.customerName || customer.customerName;
            
            customer.save((err, updatedCustomer) => {
                if (err) {
                    res.status(404).json({
                        message: 'Error in updating saved customer in database: ' + err
                    });
                } else {
                    res.status(200).json({
                        customer: customer
                    });
                }
            });
        }
    });
};

var deleteCustomer = (req, res) => {
    Customer.findByIdAndRemove(req.params.id, (err) => {
        if (err) {
            res.status(404).json({
                'message': 'error in removing the customer: ' + err
            });
        } else {
            res.status(200).json({
                'message': 'successfully remove the customer'
            });
        }
    });
}

module.exports = {
    createCustomer,
    getAllCustomer,
    getSingleCustomer,
    updateCustomer,
    deleteCustomer
};