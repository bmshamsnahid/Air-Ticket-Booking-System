var Layout = require('../../models/layout');

var createLayout = (req, res) => {
    console.log(req.body);
    
    var newLayout = new Layout(req.body);

    newLayout.save((err, savedLayout) => {
        if (err) {
            res.status(404).json({
                'message': 'Error in creating layout: ' + err
            });
        } else {
            res.status(200).json({
                'message': 'successfully saved the layout',
                'docs': savedLayout
            });
        }
    });
};

var getAllLayout = (req, res) => {
    Layout.find((err, layouts) => {
        if (err) {
            res.status(404).json({
                'message': 'error in getting layout: ' + err
            });        
        } else {
            res.status(200).json({
                'layouts': layouts
            });
        }
    });
    
};

var getSingleLayout = (req, res) => {

    Layout.findById(req.params.id, (err, retrievedLayout) => {
        if (err) {
            res.status(404).json({
                'message': 'Error in getting the layout: ' + err
            });     
        } else {
            res.status(200).json({
                layout: retrievedLayout 
            });
        }
    });
};

var updateLayout = (req, res) => {
    var id = req.params.id;

    Layout.findById(id, (err, retrievedLayout)  => {
        if (err) {
            res.status(404).json({
                'message': 'error in getting layout: ' + err
            });
        } else {
            var seatNo = req.body.seatNo;
            var seatAvailability = req.body.seatAvailability;
            var seatType = req.body.seatType;
            var seatPosition = req.body.seatPosition;
            var seatDescription = req.body.seatDescription;

            retrievedLayout.seats.push({
                seatNo: seatNo,
                seatAvailability: seatAvailability,
                seatType: seatType,
                seatPosition: seatPosition,
                seatDescription: seatDescription
            });

            retrievedLayout.save((err, savedLayout) => {
                if (err) {
                    res.status(404).json({
                        'message': 'error in saving layout: ' + err
                    });  
                } else {
                    res.status(200).json({
                        'layout': savedLayout
                    });  
                }
            });
        }
    });
};

var deleteLayot = (req, res) => {
    Layout.findByIdAndRemove(req.params.id, (err) => {
        if (err) {
            res.status(404).json({
                'message': 'error in removing the layout: ' + err
            });
        } else {
            res.status(200).json({
                'message': 'successfully remove the layout'
            });
        }
    });
};

module.exports = {
    createLayout,
    getSingleLayout,
    getAllLayout,
    updateLayout,
    deleteLayot
};