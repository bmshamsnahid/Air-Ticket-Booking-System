var Location = require('../../models/Location');

var createLocation = (req, res) => {
    var location = new Location(req.body);
    location.save((err, savedLocation) => {
        if (err) {
            res.status(200).json({
                'message': 'Error in saving the location info: ' + err
            });
        } else {
            res.status(200).json({
                'savedLocation': savedLocation
            });
        }
    });
};

var getAllLocation = (req, res) => {
    Location.find((err, locations) => {
        if (err) {
            res.status(404).json({
                'message': 'Error in get all the locations: ' + err 
            });
        } else {
            res.status(200).json({
                'locations': locations
            });
        }
    });
};

var getSingleLocation = (req, res) => {
    Location.findById(req.params.id, (err, location) => {
        if (err) {
            res.status(404).json({
                'message': 'Error in getting single location: ' + err
            });
        } else {
            res.status(200).json({
                'location': location
            });
        }
    });
};

var updateLocation = (req, res) => {
    var id = req.params.id;
    Location.findById(id, (err, foundLocation) => {
        if (err) {
            res.status(404).json({
                'error': 'Error in finding the location: ' + err
            });
        } else {

            foundLocation.locationName = req.body.locationName || foundLocation.locationName;
            foundLocation.locationCityName = req.body.locationCityName || foundLocation.locationCityName;
            foundLocation.locationState = req.body.locationState || foundLocation.locationState;
            foundLocation.locationDistrict = req.body.locationDistrict || foundLocation.locationDistrict;
            foundLocation.locationDivision = req.body.locationDivision || foundLocation.locationDivision;
            foundLocation.locationDescription = req.body.locationDescription || foundLocation.locationDescription;

            foundLocation.save((err, updatedLocation) => {
                if (err) {
                    res.status(404).json({
                        'error': 'Error in updating the location: ' + err
                    });
                } else {
                    res.status(200).json({
                        'updatedLocation': updatedLocation
                    });
                }
            });
        }
    });
};

var deleteLocation = (req, res) => {
    Location.findByIdAndRemove(req.params.id, (err) => {
        if (err) {
            res.status(404).json({
                'message': 'error in removing the location: ' + err
            });
        } else {
            res.status(200).json({
                'message': 'successfully remove the location'
            });
        }
    });
};

module.exports = {
    createLocation,
    getSingleLocation,
    getAllLocation,
    updateLocation,
    deleteLocation
};