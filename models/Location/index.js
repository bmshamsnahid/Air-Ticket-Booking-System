var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
var Schema = mongoose.Schema;

var schema = new Schema({
    locationName: {
        type: String,
        required: true
    },
    locationCityName: {
        type: String,
        required: true
    },
    locationState: {
        type: String
    },
    locationDistrict: {
        type: String
    }, 
    locationDivision: {
        type: String
    },
    locationDescription: {
        type: String
    }
});

module.exports = mongoose.model('Location', schema);