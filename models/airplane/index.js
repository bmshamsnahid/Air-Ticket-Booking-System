var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
var Schema = mongoose.Schema;

var schema = new Schema({
    airplanename: {
        type: String,
        required: true
    },
    airplaneDepertureLocation: {
        type: String,
        required: true
    },
    airplaneDestinationLocation: {
        type: String,
        required: true
    }, 
    airplaneTouchLocation: [{
        locationId: String,
        time: String
    }],
    airplaneDepertureLocationTime: {
        type: String,
        required: true
    },
    airplaneDestinationLocationTime: {
        type: String,
        required: true
    },
    airplaneDayInWeeks: [{
        day: String
    }],
    isSeatAvailable: {
        type: String,
        required: true
    }, 
    isEconomySeatAvailable: {
        type: String,
        required: true
    }, 
    isBusinessClassAvailable: {
        type: String,
        required: true
    },
    airplaneDescription: {
        type: String,
        required: true
    }
});

module.exports = mongoose.model('Airplane', schema);