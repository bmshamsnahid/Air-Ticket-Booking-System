var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
var Schema = mongoose.Schema;

var schema = new Schema({
    customerEmail: {
        type: String,
        required: true
    },
    customerPassword: {
        type: String,
        required: true
    },
    customerName : {
        type: String,
        required: true
    }
});

module.exports = mongoose.model('Customer', schema);