var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
var Schema = mongoose.Schema;

var schema = new Schema({
    layoutName: {
        type: String,
        required: true
    },
    seats: [{
        seatNo: {
            type: String,
            required: true
        },
        seatAvailability: {
            type: String,
            required: true
        },
        seatType: {
            type: String,
            required: true
        }, 
        seatPosition: {
            type: String,
            required: true
        },
        seatDescription: {
            type: String,
            requird: true
        }
    }],
    layoutDescription: {
        type: String,
        required: true
    }
});

module.exports = mongoose.model('Layout', schema);