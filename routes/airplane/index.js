var express  = require('express');
var router = express.Router();
var Airplane = require('../../models/airplane');

router.get('/', (req, res) => {

    Airplane.find((err, docs) => {
        if (err) {
            console.log('Error in Finding docs: ' + err);
            res.status(200).json({
                'message': 'Error: ' + err
            });
        } else {
            console.log('Docs are');
            console.log(JSON.stringify(docs));
            res.status(200).json({
                'Docs': docs
            });
        }
    });
});

module.exports = router;