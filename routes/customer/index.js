var express = require('express');
var router = express.Router();
var customerController = require('../../controllers/customer');
var authController = require('../../controllers/auth');

router.post('/', customerController.createCustomer);
router.get('/', authController.checkAuthentication, customerController.getAllCustomer);
router.get('/:id', authController.checkAuthentication, customerController.getSingleCustomer);
router.patch('/:id', authController.checkAuthentication, customerController.updateCustomer);
router.delete('/:id', authController.checkAuthentication, customerController.deleteCustomer);

module.exports = router;