var express = require('express');
var router = express.Router();
var Layout = require('../../models/layout');
var layoutController = require('../../controllers/layout');

router.get('/', layoutController.getAllLayout);
router.get('/:id', layoutController.getSingleLayout);
router.post('/', layoutController.createLayout);
router.patch('/:id', layoutController.updateLayout);
router.delete('/:id', layoutController.deleteLayot);

module.exports = router;