var express = require('express');
var router = express.Router();
var Location = require('../../models/Location');
var locationController = require('../../controllers/location');

router.get('/', locationController.getAllLocation);
router.post('/', locationController.createLocation);
router.patch('/:id', locationController.updateLocation);
router.delete('/:id', locationController.deleteLocation);

module.exports = router;